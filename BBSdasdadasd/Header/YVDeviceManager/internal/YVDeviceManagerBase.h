//
//  YVDeviceManagerBase.h
//  YvIMSDKManage
//  DeviceManager提供基础操作
//  Created by liwenjie on 14/12/26.
//  Copyright (c) 2014年 com.yunva.yaya. All rights reserved.
//

// 是否需要语音识别,打开:需要，注释:不需要
//#define kOpenRecognizerDevice
// 是否需要监听对象数组,打开:需要，注释:不需要
//#define kDelegateArray

#import <Foundation/Foundation.h>
//#import "YVDeviceManagerDelegate.h"

@protocol YVDeviceManagerBase <NSObject>


@end

