
/*!
 @header YvChatManageDelegate.h
 @abstract 关于ChatManager的异步回调协议
 @author com.yunva.yaya Inc.
 @version 1.00 2014/12/24. Creation (1.00)
 */

#import <Foundation/Foundation.h>
#import "YVChatManageDelegateAll.h"

/**
 *  本协议主要处理聊天时的一些回调操作（如发送消息成功、收到消息、收到添加好友邀请等消息时的回调操作）
 */
@protocol YVChatManageDelegate <YVChatManageDelegateAll>


@end

