//
//  YvChatManageDelegateBase.h
//  YvIMSDKManage
//
//  Created by liwenjie on 14/12/24.
//  Copyright (c) 2014年 com.yunva.yaya. All rights reserved.
//

#define MARK_DEPRECATED __attribute__((deprecated))


#import <Foundation/Foundation.h>
#import "YvIMCMDDefHeaders.h"

/*!
 @protocol
 @brief 聊天回调基类
 @discussion 所有聊天回调接口皆派生于此基类
 */
@protocol YVChatManageDelegateBase <NSObject>


@end

