//
//  YvIMNetStateNotify.h
//  YvIMSDKAPITestDemo
//
//  Created by liwenjie on 15-5-20.
//  Copyright (c) 2015年 com.yunva.yaya. All rights reserved.
//

#import "YvBackBase.h"
@interface YvIMNetStateNotify : YvBackBase
@property(assign, nonatomic) UInt8 state;
@end
